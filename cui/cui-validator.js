'use strict'

module.exports = {
  textContainsCUIName: function(text) {
      const CUINamesArrived = [
        "David Buenfil García",
        "Carlos Jauriga Barbosa",
        "Erick Meéndez Carrillo",
        "Ángel González Olivera",
        "Juan José Quijano Mendoza",
        "Liliana Zapata Carrizales",
        "Nancy Diaz Cardós",
        "Adriana Hernández Álvarez",
        "Kevin Cocom Uc"
      ]

      var hasCUIArrived = false
      for (let name of CUINamesArrived) {
          if (text.toUpperCase().indexOf(name.toUpperCase()) !== -1) {
              hasCUIArrived = true
          }
      }
      return hasCUIArrived
  }
}
