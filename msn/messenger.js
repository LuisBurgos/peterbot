'use strict'

const request = require('request')
const token = process.env.FB_PAGE_ACCESS_TOKEN

module.exports = {

    sendTextCard: function(sender, cardContent) {
        request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {
                access_token: token
            },
            method: 'POST',
            json: {
                recipient: {
                    id: sender
                },
                message: cardContent
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var recipientId = body.recipient_id;
                var messageId = body.message_id;

                console.log("Successfully sent generic message with id %s to recipient %s", messageId, recipientId);
            } else {
                console.error("Unable to send message.");
                console.error(response);
                console.error(error);
            }
        });
    },

    sendImageFromURL: function(sender, imageURL) {
        let messageData = {
            "attachment": {
                "type": "image",
                "payload": {
                    "url": imageURL
                }
            }
        }

        request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {
                access_token: token
            },
            method: 'POST',
            json: {
                recipient: {
                    id: sender
                },
                message: messageData
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var recipientId = body.recipient_id;
                var messageId = body.message_id;
                console.log("Successfully sent generic message with id %s to recipient %s", messageId, recipientId);
            } else {
                console.error("Unable to send message.");
                console.error(response);
                console.error(error);
            }
        });
    },

    sendTextMessageWithCallback: function(sender, textToSend, callback) {
        let messageData = {
            recipient: {
                id: sender
            },
            message: {
                text: textToSend
            }
        }

        request({
            uri: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {
                access_token: token
            },
            method: 'POST',
            json: messageData

        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                callback()
            } else {
                console.error("Unable to send message.");
                console.error(response);
                console.error(error);
            }
        });
    },

    sendTextMessage: function(sender, textToSend) {
        let messageData = {
            recipient: {
                id: sender
            },
            message: {
                text: textToSend
            }
        }

        request({
            uri: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {
                access_token: token
            },
            method: 'POST',
            json: messageData

        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var recipientId = body.recipient_id;
                var messageId = body.message_id;

                console.log("Successfully sent generic message with id %s to recipient %s", messageId, recipientId);
            } else {
                console.error("Unable to send message.");
                console.error(response);
                console.error(error);
            }
        });
    },

    sendFirstMessage: function(sender) {
        const CUIOption = {
            "title": "Credencial Universitaria Inteligente",
            "payload": "OPTION_CUI"
        }
        const nextScholarshipPaymentOtion = {
            "title": "Becas: Calendario de Pagos",
            "payload": "OPTION_SCHOLARSHIP"
        }
        const mobilityTripOption = {
            "title": "2017: Movilidad Estudiantil",
            "payload": "OPTION_MOBILITY"
        }

        let messageData = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "¿En qué puedo ayudarte?",
                            "subtitle": "Por favor selecciona una opción",
                            "image_url": "http://www.unidadacademica.uady.mx/imagenes/faceCAE.png",
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": CUIOption.title,
                                    "payload": CUIOption.payload
                                }, {
                                    "type": "postback",
                                    "title": nextScholarshipPaymentOtion.title,
                                    "payload": nextScholarshipPaymentOtion.payload
                                }, {
                                    "type": "postback",
                                    "title": mobilityTripOption.title,
                                    "payload": mobilityTripOption.payload
                                }
                            ]
                        }
                    ]
                }
            }
        }

        request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {
                access_token: token
            },
            method: 'POST',
            json: {
                recipient: {
                    id: sender
                },
                message: messageData
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var recipientId = body.recipient_id;
                var messageId = body.message_id;

                console.log("Successfully sent generic message with id %s to recipient %s", messageId, recipientId);
            } else {
                console.error("Unable to send message.");
                console.error(response);
                console.error(error);
            }
        });
    }
};
