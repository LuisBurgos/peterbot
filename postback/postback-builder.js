'use strict'

const CUIValidator = require('../cui/cui-validator.js')

module.exports = {
    mobilityFirst: function() {
        const inProcess = {
            "title": "Estoy en el proceso para Enero-Junio 2017",
            "payload": "OPTION_MOBILITY PROCESS"
        }
        const justInformation = {
            "title": "No, quiero información del proceso",
            "payload": "OPTION_MOBILITY INFO"
        }

        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "¿Estas en el Proceso Actual de movilidad?",
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": inProcess.title,
                                    "payload": inProcess.payload
                                }, {
                                    "type": "postback",
                                    "title": justInformation.title,
                                    "payload": justInformation.payload
                                }
                            ]
                        }
                    ]
                }
            }
        }
    },

    mobilityProcess: function() {
        const cardReceived = {
            "title": "Recibí mi carta de aceptación",
            "payload": "OPTION_MOBILITY PROCESS CARD"
        }
        const cardNotReceived = {
            "title": "No he recibido mi carta de aceptaciónón",
            "payload": "OPTION_MOBILITY PROCESS NOTRECEIVED"
        }

        const lastNews = {
            "title": "Últimos avisos",
            "payload": "OPTION_MOBILITY PROCESS NEWS"
        }

        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "¿Cúal es tu situación actual?",
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": cardReceived.title,
                                    "payload": cardReceived.payload
                                }, {
                                    "type": "postback",
                                    "title": cardNotReceived.title,
                                    "payload": cardNotReceived.payload
                                }, {
                                    "type": "postback",
                                    "title": lastNews.title,
                                    "payload": lastNews.payload
                                }
                            ]
                        }
                    ]
                }
            }
        }
    },

    mobilityInformation: function() {
        const requirements = {
            "title": "Requisitos necesarios",
            "payload": "OPTION_MOBILITY INFO REQS"
        }
        const schoolList = {
            "title": "Lista de escuelas",
            "payload": "OPTION_MOBILITY INFO SCHOOLS"
        }

        const scholarshipsAvailable = {
            "title": "Becas disponibles",
            "payload": "OPTION_MOBILITY INFO SCHOLARSHIPAVAILABLE"
        }

        //Verify to inclusion. Buttons has limit of 3.
        const languagesNeeded = {
            "title": "¿Qué comprobante de idioma necesito?",
            "payload": "OPTION_MOBILITY INFO LANG"
        }

        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "¿Qué información necesitas?",
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": requirements.title,
                                    "payload": requirements.payload
                                }, {
                                    "type": "postback",
                                    "title": schoolList.title,
                                    "payload": schoolList.payload
                                }, {
                                    "type": "postback",
                                    "title": scholarshipsAvailable.title,
                                    "payload": scholarshipsAvailable.payload
                                }
                            ]
                        }
                    ]
                }
            }
        }
    },

    requirements: function() {
        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "Requisitos",
                            "subtitle": "Aquí puedes ver los requisitos, estos corresponden a la convocatoria de excención de cuotas y pagos que es obligatoria para cualquiera que desee hacer movilidad.",
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "http://www.saie.uady.mx/media/docs/pimes/PIMES%20AGO16-2/CONVOCATORIA%20BECAS%20DE%20EXENCION%20DE%20CUOTAS%20DE%20MOVILIDAD.pdf",
                                    "title": "Ver requisitos"
                                }, {
                                    "type": "postback",
                                    "title": "Regresar",
                                    "payload": "OPTION_MOBILITY INFO"
                                }
                            ]
                        }
                    ]
                }
            }
        }
    },

    schools: function() {
        const byScholarship = {
            "title": "Por Beca",
            "payload": "OPTION_MOBILITY INFO SCHOOLS BYBECA"
        }
        const byAgreement = {
            "title": "Convenios",
            "payload": "OPTION_MOBILITY INFO SCHOOLS BYAGREEMENT"
        }

        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "¿Qué tipo de escuelas?",
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": byScholarship.title,
                                    "payload": byScholarship.payload
                                }, {
                                    "type": "postback",
                                    "title": byAgreement.title,
                                    "payload": byAgreement.payload
                                }
                            ]
                        }
                    ]
                }
            }
        }
    },

    scholarships: function() {
        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "No hay convocatorias",
                            "subtitle": "Ahora no hay ninguna convocatoria de beca abierta para realizar movilidad. Las nuevas convocatorias salen en Enero 2017.",
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "http://www.saie.uady.mx/movilidad/index.php",
                                    "title": "Convocatorias anteriores"
                                }, {
                                    "type": "postback",
                                    "title": "Regresar",
                                    "payload": "OPTION_MOBILITY INFO"
                                }
                            ]
                        }
                    ]
                }
            }
        }
    },

    byScholarships: function() {
        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "ANUIES",
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "http://www.saie.uady.mx/media/docs/pimes/PIMES%20AGO16-2/INSTITUCIONES%20AFILIADAS%20A%20ANUIES.pdf",
                                    "title": "Descargar listado ANUIES"
                                }, {
                                    "type": "postback",
                                    "title": "Regresar",
                                    "payload": "OPTION_MOBILITY INFO"
                                }
                            ]
                        }, {
                            "title": "CUMEX",
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "http://www.saie.uady.mx/media/docs/pimes/PIMES%20AGO16-2/INSTITUCIONES%20AFILIADAS%20A%20CUMEX.pdf",
                                    "title": "Descargar listado CUMEX"
                                }, {
                                    "type": "postback",
                                    "title": "Regresar",
                                    "payload": "OPTION_MOBILITY INFO"
                                }
                            ]
                        }, {
                            "title": "ECOES",
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "http://www.saie.uady.mx/media/docs/pimes/PIMES%20AGO16-2/INSTITUCIONES%20AFILIADAS%20A%20ECOES.pdf",
                                    "title": "Descargar listado ECOES"
                                }, {
                                    "type": "postback",
                                    "title": "Regresar",
                                    "payload": "OPTION_MOBILITY INFO"
                                }
                            ]
                        },
                    ]
                }
            }
        }
    },

    byAgreement: function() {
        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "Ver lista de convenios para movilidad",
                            "subtitle": "No olvides que la lista de escuelas se actualiza cada semestre, úsalo solo como referencia",
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "http://www.saie.uady.mx/media/docs/pimes/PIMES%20AGO16-2/Anexo1-Convenios%20de%20Movilidad%20-CARTA.pdf ",
                                    "title": "Lista de convenios"
                                }, {
                                    "type": "postback",
                                    "title": "Regresar",
                                    "payload": "OPTION_MOBILITY INFO"
                                }
                            ]
                        }
                    ]
                }
            }
        }
    },

    cuiFirst: function() {
        const enterName = {
            "title": "Ingresar nombre",
            "payload": "OPTION_CUI ENTERNAME"
        }
        const backToMainOption = {
            "title": "Regresar al inicio",
            "payload": "OPTION_CUI MAIN"
        }

        return {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "Nombre de estudiante",
                            "subtitle": "Por favor ingresa tu nombre de estudiante para verificar que tu Crendencial Universitaria Inteligente esté lista para que pases por ella. Ejemplo: José Luis Pérez López",
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": enterName.title,
                                    "payload": enterName.payload
                                }, {
                                    "type": "postback",
                                    "title": backToMainOption.title,
                                    "payload": backToMainOption.payload
                                }
                            ]
                        }
                    ]
                }
            }
        }
    }
}
