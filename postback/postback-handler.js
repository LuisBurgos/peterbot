'use strict'

const Messenger = require('../msn/messenger.js')
const PostbackBuilder = require('../postback/postback-builder.js')
const CUIValidator = require('../cui/cui-validator.js')

var Globals = require('../globals.js')

var self = module.exports = {

    handle: function(sender, text, isNonPostback) {

        if (isNonPostback) {
            if (Globals.expectedPostback.indexOf("OPTION_CUI ENTERNAME") !== -1){
                var messageToSend = "Lo siento! Tu nombre no se encuentra en la lista de Credenciales Universitarias Intelegentes. Sigue al pendiente."
                var foundName = CUIValidator.textContainsCUIName(text)
                if (foundName) {
                    messageToSend = "Felicidades! Encontramos tu nombre en la lista. Puedes pasar por tu Credencia Universitaria Inteligente al CAE"
                }
                Messenger.sendTextMessageWithCallback(sender, messageToSend, function(){
                    console.log("Remove expecting message");
                    Globals.isExpectingPostback = !foundName
                    Globals.expectedPostback = foundName ? "" : "OPTION_CUI ENTERNAME"
                })
            }
        } else {
            if (text.toUpperCase().indexOf("OPTION_MAIN") !== -1) {
                Messenger.sendFirstMessage(sender)
            } else if (text.toUpperCase().indexOf("OPTION_CUI") !== -1) {
                self.handleCUI(sender, text)
            } else if (text.toUpperCase().indexOf("OPTION_SCHOLARSHIP") !== -1) {
                self.handleScholarship(sender, text)
            } else if (text.toUpperCase().indexOf("OPTION_MOBILITY") !== -1) {
                self.handleMobility(sender, text)
            }
        }
    },

    handleCUI: function(sender, text){
        if (text.toUpperCase().indexOf("ENTERNAME") !== -1) {
            Messenger.sendTextMessageWithCallback(sender, "Por favor ingresa tu nombre. Ejemplo: José Luis Pérez López", function(){
                console.log("Setting expecting message");
                Globals.isExpectingPostback = true
                Globals.expectedPostback = "OPTION_CUI ENTERNAME"
            })
        } else if (text.toUpperCase().indexOf("MAIN") !== -1) {
            Messenger.sendFirstMessage(sender)
        } else {
            Messenger.sendTextMessage(sender, "Elegiste entregas de la Credencial Universitaria Inteligente")
            Messenger.sendTextCard(sender, PostbackBuilder.cuiFirst())
        }
    },

    handleScholarship: function(sender, text) {
        Messenger.sendTextMessage(sender, "Te envío una imagen del calendario de pagos para las siguientes becas: UADY-NFY, UADY-FEDY, UADY, Apoyo a Madres y Padres Jóvenes Universitarios")
        Messenger.sendImageFromURL(sender, "https://cloud.githubusercontent.com/assets/7050120/20916729/d3dd87da-bb52-11e6-9e28-6e300cac5a23.png")
    },

    handleMobility: function(sender, text) {
        console.log("OPTION_MOBILITY");
        if (text.toUpperCase().indexOf("PROCESS") !== -1) {

            if (text.toUpperCase().indexOf("CARD") !== -1) {
                Messenger.sendTextMessage(sender, "Envíale tu carta de aceptaciónón a la maestra Norma Navarrete al correo ndMara@correo.uady.mx . Comunícate con la escuela destino para pedir las fechas en las que debes presentarte.\n\nAcude a la junta de 12 diciembre e Informa tu situaciónón.")
            } else if (text.toUpperCase().indexOf("NOTRECEIVED") !== -1) {
                Messenger.sendTextMessage(sender, "La maestra Norma Navarrete se comunicará contigo apenas tenga tu carta de aceptación.\n\nNo olvides revisar tu bandeja spam cuidadosamente, por que ocurre a menudo que se encuentran en correo no deseado.")
            } else if (text.toUpperCase().indexOf("NEWS") !== -1) {
                Messenger.sendTextMessage(sender, "La junta informativa se realizará el 12 de diciembre de 9 am a 1 pm.\n\nLos resultados de las becas serán publicados a mediados de Enero.")
            } else {
                Messenger.sendTextCard(sender, PostbackBuilder.mobilityProcess())
            }

        } else if (text.toUpperCase().indexOf("INFO") !== -1) {
            if (text.toUpperCase().indexOf("REQS") !== -1) {
                Messenger.sendTextCard(sender, PostbackBuilder.requirements())
            } else if (text.toUpperCase().indexOf("SCHOOLS") !== -1) {
                if(text.toUpperCase().indexOf("BYBECA") !== -1){
                    Messenger.sendTextCard(sender, PostbackBuilder.byScholarships())
                } else if (text.toUpperCase().indexOf("BYAGREEMENT") !== -1) {
                    Messenger.sendTextCard(sender, PostbackBuilder.byAgreement())
                } else {
                    Messenger.sendTextCard(sender, PostbackBuilder.schools())
                }
            } else if (text.toUpperCase().indexOf("SCHOLARSHIPAVAILABLE") !== -1) {
                Messenger.sendTextCard(sender, PostbackBuilder.scholarships())
            } else {
                Messenger.sendTextCard(sender, PostbackBuilder.mobilityInformation())
            }
        } else {
            console.log("OPTION_MOBILITY");
            Messenger.sendTextMessage(sender, "Elegiste obtener información del proceso de movilidad estudiantil del perido Enero-Julio 2017")
            Messenger.sendTextCard(sender, PostbackBuilder.mobilityFirst())
        }
    }
}
