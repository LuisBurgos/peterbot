'use strict'

module.exports = {
    textContainsQuestion: function(text) {
        const hQuestions = [
            "que",
            "como",
            "quien",
            "cuando",
            "donde",
            "cual",
            "cuales",
            "¿",
            "?",
            "qué",
            "cómo",
            "cúando",
            "dónde",
            "quién",
            "porqué",
            "cuál",
            "cuáles"
        ]

        var hasHQuestion = false
        for (let question of hQuestions) {
            if (text.toUpperCase().indexOf(question.toUpperCase()) !== -1) {
                hasHQuestion = true
            }
        }
        return hasHQuestion
    },

    textContainsGreeting: function(text) {
        const commonGreetings = [
            "hola",
            "hello",
            "hi",
            "alló",
            "que tal",
            "como estas"
        ]

        var hasGreeting = false
        for (let greeting of commonGreetings) {
            if (text.toUpperCase().indexOf(greeting.toUpperCase()) !== -1) {
                hasGreeting = true
            }

            var complexGreeting = greeting + " peter"
            if (text.toUpperCase().indexOf(complexGreeting.toUpperCase()) !== -1) {
                hasGreeting = true
            }
        }
        return hasGreeting
    }

}
